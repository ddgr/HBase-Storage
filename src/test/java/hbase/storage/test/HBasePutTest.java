package hbase.storage.test;

import cn.ngsoc.hbase.HBase;
import cn.ngsoc.hbase.util.HBaseUtil;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * HBase 测试类 Created by babylon on 2016/11/29.
 */
public class HBasePutTest {
	private String tabName = "test_hbase";

	@Before
	public void init() {
		HBaseUtil.init("10.10.4.36");
	}

	@Test
	public void testPut() {
		Put put = new Put(Bytes.toBytes("rowKey"));
		put.addColumn(Bytes.toBytes("events"), Bytes.toBytes("severity"), Bytes.toBytes(new Random().nextInt(10) + ""));
		HBase.put(tabName, put, true);
	}
	
	@Test
	public void testListPut() {
		List<Put> putList = new ArrayList<Put>();
		for(int i=0; i<20000; i++){
			String tag = UUID.randomUUID().toString();
			Put put = new Put(Bytes.toBytes(tag));
			for(int j=0; j<20; j++){
				put.addColumn(Bytes.toBytes("events"), Bytes.toBytes("severity"+j), Bytes.toBytes(new Random().nextInt(10) + ""));
			}
			putList.add(put);
		}
		HBase.put(tabName, putList, true);
	}

	@Test
	public void testGet() {
		Result result = HBaseUtil.getRow(tabName, HBase.generateRowkey("rowKey"));
		HBaseUtil.formatRow(result.raw());
	}

	@Test
	public void testCreateTable() {
		try {
			HBaseUtil.createTable(tabName, new String[] { "events" }, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package hbase.storage.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import cn.ngsoc.hbase.HBase;
import cn.ngsoc.hbase.util.HBaseUtil;


public class TestHBase {
	
	static {
		HBaseUtil.init("10.10.4.36");
	}
	
	private static final String tabName = "test_hbase";
	
	
	public static void main(String[] a) {
		List<Put> putList = new ArrayList<Put>();
		for (int i = 0; i < 20000; i++) {
			String tag = UUID.randomUUID().toString();
			Put put = new Put(Bytes.toBytes(tag));
			for (int j = 0; j < 20; j++) {
				put.addColumn(Bytes.toBytes("events"), Bytes.toBytes("severity" + j), Bytes.toBytes(new Random().nextInt(10) + ""));
			}
			putList.add(put);
		}
		HBase.put(tabName, putList, true);
		System.exit(0);
	}
}

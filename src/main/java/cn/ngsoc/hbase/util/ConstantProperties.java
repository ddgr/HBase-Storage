package cn.ngsoc.hbase.util;

/**
 * Created by zhenjie.wang on 2015/8/19.
 */
public class ConstantProperties {
    public static String DB = "configuration/db.properties";
    public static String DB_PROP = "configuration/dbone.properties";
    public static String SOC_PROP = "configuration/soc.properties";
    public static String COMMON_PROP = "configuration/common.properties";
    public static String URL_PROP = "configuration/url.properties";
    public static String URL_SOC_PROP = "configuration/url_soc.properties";
    
    public static String NO_SPLIT_HBASE_TABLE = "dbone.wdd_session,dbone.wdd_sql";
}
